import React from 'react';
import {
	Route,
	BrowserRouter as Router,
} from 'react-router-dom';
import 'react-md/dist/react-md.deep_purple-pink.min.css';

import LoginContainer from '../containers/Login/LoginContainer';
import DashboardContainer from '../containers/Dashboard/DashboardContainer';

const App = () => (
	<Router>
		<div>
			<Route exact path="/" component={LoginContainer} />
			<Route exact path="/account" component={DashboardContainer}/>
		</div>
	</Router>
);

export default App;
