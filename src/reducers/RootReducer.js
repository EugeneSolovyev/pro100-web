import {
	combineReducers
} from 'redux';
import {
	routerReducer
} from 'react-router-redux';

import login from '../containers/Login/LoginReducer';

export default combineReducers({
	router: routerReducer,
	login
	//.. other reducers
});