import React from 'react';
import {
	bindActionCreators
} from 'redux';
import {
	connect
} from 'react-redux';

import {
	Card,
	CardTitle,
	CardText,
	CardActions,
	Button,
	FileInput,
	Avatar,
	TextField,
} from 'react-md';
import DEFAULT_AVATAR from '../../content/man.png';

const mapStateToProps = state => ({
	user: _.get(state, 'login', {}),
});

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

class ProfileContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			...this.props.user,
			lastVisit: 'June 22, 2018',
		};
		this.changeField = this.changeField.bind(this);
	}

	changeField(_, evt) {
		this.setState({
			[evt.target.name]: evt.target.value
		})
	}

	render() {
		const {
			login,
			email,
			firstName,
			lastName,
			avatar,
			lastVisit,
		} = this.state;

		return (
			<div>
				<Card>
					<CardTitle
						avatar={<Avatar src={avatar || DEFAULT_AVATAR}/>}
						title={login}
						subtitle={lastVisit}
						role="presentation"/>
					<div className='md-divider-border md-divider-border--top'>
						<TextField
							name='firstName'
							type='text'
							id='Profile_First_Name'
							label='First Name'
							lineDirection='center'
							value={firstName || ''}
							onChange={this.changeField}/>
						<TextField
							name='lastName'
							type='text'
							id='Profile_First_Name'
							label='Last Name'
							lineDirection='center'
							value={lastName || ''}
							onChange={this.changeField}/>
						<TextField
							name='email'
							type='email'
							id='Profile_First_Name'
							label='Email'
							lineDirection='center'
							value={email || ''}
							onChange={this.changeField}/>
						<FileInput
							label='Load photo'
							id="image-input-3"
							accept="image/*"
							name="images"
							primary />
					</div>
				</Card>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);