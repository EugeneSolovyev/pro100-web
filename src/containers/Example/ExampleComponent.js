import React from 'react';
import loremIpsum from 'lorem-ipsum'

class Example extends React.PureComponent {

	render() {
		const text = loremIpsum({ units: 'paragraphs', count: this.props.count });
		return (
			<div>
				<p>{text}</p>
			</div>
		);
	}
}

export default Example;