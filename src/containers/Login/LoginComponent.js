import React from 'react';
import {
	Grid,
	Card,
	TextField,
} from 'react-md';
import LoginControlPanel from './LoginControlPanel';

import logo from '../../content/logo-example.jpg';

import './Login.styl';

class LoginComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			login: '',
			password: '',
			email: '',
			isSignUp: false,
		};
		this.LogIn = this.LogIn.bind(this);
		this.SignUp = this.SignUp.bind(this);
		this.Google = this.Google.bind(this);
		this.changeFieldLogin = this.changeFieldLogin.bind(this);
		this.changeFieldPassword = this.changeFieldPassword.bind(this);
		this.changeFieldEmail = this.changeFieldEmail.bind(this);
		this.wantToSignUp = this.wantToSignUp.bind(this);
	}

	LogIn() {
		if (_.isEmpty(this.state.login) || _.isEmpty(this.state.password)) return;
		this.props.login(this.state);
	}

	Google() {
		this.props.google();
	}

	SignUp() {
		if (_.isEmpty(this.state.login) || _.isEmpty(this.state.password) || _.isEmpty(this.state.email)) return;
		this.props.signup(this.state);
	}

	changeFieldLogin(login) {
		this.setState({
			login
		});
	}

	changeFieldPassword(password) {
		this.setState({
			password
		});
	}

	changeFieldEmail(email) {
		this.setState({
			email
		});
	}

	wantToSignUp(e) {
		e.preventDefault();
		this.setState({
			isSignUp: !this.state.isSignUp,
		});
	}

	render() {
		let EmailField = this.state.isSignUp
			? <TextField label='Email'
						 id='LoginComponent__email'
						 value={this.state.email}
						 onChange={this.changeFieldEmail}
						 type='email'/>
			: null;

		let SignUpLink = !this.state.isSignUp
			? <p className='LoginComponent__sign-up-link'>Not registered yet?<a href="javascript:void(0)" onClick={this.wantToSignUp}>Sign Up!</a></p>
			: <p className='LoginComponent__sign-up-link'>I've already registred! <a href="javascript:void(0)" onClick={this.wantToSignUp}>Login!</a></p>;


		return (
			<Grid className="Login">
				<Card className='Login__card md-cell md-cell--6 md-cell--8-tablet'>
					<img src={logo} className='LoginComponent__logo'/>
					<TextField label='Login'
							   id='LoginComponent__login'
							   value={this.state.login}
							   onChange={this.changeFieldLogin}/>
					<TextField label='Password'
							   id='LoginComponent__password'
							   value={this.state.password}
							   onChange={this.changeFieldPassword}
							   type='password'/>
					{EmailField}
					<LoginControlPanel isSignUp={this.state.isSignUp}
									   SignUp={this.SignUp}
									   LogIn={this.LogIn}
									   Google={this.Google}/>
					{SignUpLink}
				</Card>
			</Grid>
		);
	}
}

export default LoginComponent;