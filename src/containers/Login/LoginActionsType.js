const LoginActionsTypes = {
	LOG_IN: 'LOG_IN',
	SIGN_UP: 'SIGN_UP',
};

export default LoginActionsTypes;