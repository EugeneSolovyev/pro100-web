import React from 'react';
import {
	Button
} from 'react-md';
import axios from "axios/index";
const mainUrl = 'http://localhost:8888';

class LoginControlPanel extends React.PureComponent {
	render() {
		return (
			<div className='LoginComponent__control-panel'>
				<Button raised
						primary
						className='Login__button'
						onClick={
							this.props.isSignUp ? this.props.SignUp : this.props.LogIn
						}>
					{
						this.props.isSignUp ? 'Sign Up' : 'Log In'
					}
				</Button>
				<Button raised
						primary
						onClick={this.props.Google}
						className='Login__button'>Google</Button>
			</div>
		);
	}
}

export default LoginControlPanel;