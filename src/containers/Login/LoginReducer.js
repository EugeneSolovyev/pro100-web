import LoginActionsTypes from './LoginActionsType'

const INITIAL_STATE = {
	login: 'Eugene',
	password: '123',
	id: 1
};

export default (state = INITIAL_STATE, action) => {
	let updated = {};
	switch (action.type) {
		case LoginActionsTypes.SIGN_UP:
		case LoginActionsTypes.LOG_IN:
			updated = action.payload;
	}
	return updated;
}