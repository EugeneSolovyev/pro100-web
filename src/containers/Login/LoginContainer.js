import React from 'react';
import {
	bindActionCreators
} from 'redux';
import {
	connect
} from 'react-redux';

import LoginComponent from './LoginComponent';
import {
	Login,
	Google,
	SignUp,
	isAuthenticated,
} from './LoginActions';

class LoginContainer extends React.Component {
	constructor(props) {
		super(props);
		this.login = this.login.bind(this);
		this.google = this.google.bind(this);
		this.signup = this.signup.bind(this);
	}

	login(state) {
		this.props.Login(
			state,
			this.props.history
		);
	}

	google() {
		this.props.Google();
	}

	signup(state) {
		this.props.SignUp(
			state,
			this.props.history
		);
	}

	componentWillMount() {
		this.props.isAuthenticated();
	}

	render() {
		return (
			<div className='Root'>
				<LoginComponent login={this.login} google={this.google} signup={this.signup}/>
			</div>
		);
	};
}

const mapStateToProps = state => ({
	login: state.login,
	google: state.google,
	signup: state.signup,
});

const mapDispatchToProps = dispatch => bindActionCreators({
	Login,
	Google,
	SignUp,
	isAuthenticated,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);