import API from '../../API/api';

import LoginActionsTypes from './LoginActionsType';

const isAuthenticated = (history) => {
	return dispatch => API.get('user/current')
		.then(response => {
			console.log(response);
		})
		.catch(error => {
			console.log(error);
		});
};

const Login = (state, history) => {
	return dispatch => API.post('auth/login', {
		login: state.login,
		password: state.password
	})
		.then(response => {
			dispatch({
				type: LoginActionsTypes.LOG_IN,
				payload: _.get(response, 'data.data', {})
			});
			history.push('/account');
		}, reject => {
			console.log(reject);
		});
};

const Google = () => {
	window.location = 'http://localhost:8888/auth/google';
};

const SignUp = (state, history) => {
	return dispatch => API.post('user/create', {
		login: state.login,
		password: state.password,
		email: state.email
	})
		.then(resolve => {
			dispatch({
				type: LoginActionsTypes.SIGN_UP,
				payload: _.get(resolve, 'data.data', {})
			});
			history.push('/account');
		});
};

export {
	Login,
	Google,
	SignUp,
	isAuthenticated
};