/* eslint-disable react/no-array-index-key */
import React, { PureComponent } from 'react';
import {
	NavigationDrawer,
	FontIcon
} from 'react-md';

import Example from '../../Example/ExampleComponent';
import ProfileContainer from '../../Profile/ProfileContainer';

const inboxListItems = [{
	key: 1,
	primaryText: 'Profile',
	content: <ProfileContainer />,
	title: 'Profile'
}, {
	key: 2,
	primaryText: 'Friends',
	content: <Example count={3} />,
	title: 'Friends'
}, {
	key: 3,
	primaryText: 'Settings',
	content: <Example count={10} />,
	title: 'Settings'
}];

class DashboardNavigationComponent extends PureComponent {
	constructor(props) {
		super(props);

		this.navItems = inboxListItems.map((item) => {
			if (item.divider) {
				return item;
			}

			return {
				...item,
				onClick: () => this.setPage(item),
			};
		});

		this.state = {
			renderNode: null,
			visible: false,
			card: {
				title: inboxListItems[0].title,
				key: inboxListItems[0].key,
				page: inboxListItems[0].primaryText,
				content: inboxListItems[0].content
			}
		};
	}

	setPage = (card) => {
		this.navItems = this.navItems.map((item) => {
			if (item.divider) {
				return item;
			}

			return { ...item, active: item.key === card.key };
		});

		this.setState({card});
	};

	render() {
		const {
			renderNode
		} = this.state;
		const {
			title,
			content,
		} = this.state.card;
		return (
			<div>
				<NavigationDrawer
					renderNode={renderNode}
					navItems={this.navItems}
					mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY_MINI}
					tabletDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT_MINI}
					desktopDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT_MINI}
					toolbarTitle={title}
					temporaryIcon={<FontIcon>menu</FontIcon>}
					persistentIcon={<FontIcon>arrow_back_ios</FontIcon>}
					contentId="main-demo-content"
					contentClassName="md-grid"
				>
					<section className="md-text-container md-cell md-cell--12">
						{content}
					</section>
				</NavigationDrawer>
			</div>
		);
	}
}


export default DashboardNavigationComponent;