import React from 'react';
import PropTypes from 'prop-types';
import {
	Button
} from 'react-md';

const Navigation = ({className}) => <Button icon className={className}>menu</Button>;

Navigation.propTypes = {
	className: PropTypes.string,
};

export default Navigation;