var CACHE_NAME = 'pro-100_service-worker_v1';
var CACHES_URLS = [
	'/',
	'/index.html',
	'/source.css',
	'/source.js'
];

self.addEventListener('install', function(event) {
	event.waitUntil(
		caches.open(CACHE_NAME).then(function(cache) {
			return cache.addAll(CACHES_URLS);
		})
	);
});

self.addEventListener('fetch', function(event) {
	console.log(event.request.url);
	event.respondWith(
		caches.match(event.request).then(function(response) {
			return response || fetch(event.request);
		})
	);
});

self.addEventListener('activate', function(event) {
	console.log('activate', event);
});